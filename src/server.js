const fun = require('./funcoes');
const express = require('express');
const bodyParse = require('body-parser');
const app = express();
const porta = 8002;
app.use(bodyParse.urlencoded({ extended: true }));
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('../db/banco.sqlite3')

module.exports = db;


app.get('/', (req, res) => {
    res.sendFile(__dirname+'/html/'+ '/principal.html')
    console.log('Executando pagina html')
});
app.get('/despesas',(req,res)=>{
    res.sendFile(__dirname+'/html/'+ '/despesas.html')
    console.log('Executando pagina html despesas')
})

app.get('/receitas',(req,res)=>{
    res.sendFile(__dirname+'/html/'+ '/receitas.html')
    console.log('Executando pagina html receitas')
})

app.post('/enviarDespesas', (req, res) => {
    let valor = req.body.valor;
    let despesa = req.body.descricao;
    let data = req.body.data;
    let pagamento = req.body.formadepagemento;
    let parcelas = req.body.despesa_fixa;
    db.run(`INSERT INTO despesas (valor, despesa, data, pagamento, parcelas) VALUES (?, ?, ?, ?, ?)`,
        [valor, despesa, data, pagamento, parcelas ? true : false], (err) => {
            if (err) {
                console.error(err.message);
                return res.status(500).send('Erro ao inserir dados no banco de dados');
            }
            console.log('Dados inseridos com sucesso!');
            return res.status(200).send('Dados inseridos com sucesso!');
        }
    );
});
app.post('/enviarReceitas', (req, res) => {
    let valor = req.body.valor;
    let despesa = req.body.descricao;
    let data = req.body.data;
    let pagamento = req.body.formadepagemento;
    let parcelas = req.body.despesa_fixa;
    db.run(`INSERT INTO despesas (valor, despesa, data, pagamento, parcelas) VALUES (?, ?, ?, ?, ?)`,
        [valor, despesa, data, pagamento, parcelas ? true : false], (err) => {
            if (err) {
                console.error(err.message);
                return res.status(500).send('Erro ao inserir dados no banco de dados');
            }
            console.log('Dados inseridos com sucesso!');
            return res.status(200).send('Dados inseridos com sucesso!');
        }
    );
});

db.serialize(()=>{
    db.run(`
        CREATE TABLE IF NOT EXISTS despesas(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            valor REAL,
            despesa TEXT,
            data TEXT,
            pagamento TEXT,
            parcelas BOOLEAN
        );
    `), function(err) {
        if (err) {
            console.log(err.message);
            return;
        }
        console.log('Tabela criada com sucesso!');
    }
});
db.serialize(()=>{db.all('SELECT * FROM despesas',[],(err, rows) => {
    if (err) {
      console.log(err);
    } else {
      console.log(rows);
    }});})


    process.on('SIGINT', () => {
        db.close(() => {
            console.log('Conexão com o banco de dados encerrada.');
            process.exit(0);
        });
    });


app.listen(porta, () => {
    console.log(`Escutando na porta: ${porta}`)
});