const server = require('./server')
var despesas = [];
var receita = [];
function factoryDespesas(valor,fonte,data,pagamento,parcelas){
  return {valor,fonte,data,pagamento,parcelas}
}
function factoryReceitas(valor,fonte,data){
  return {valor,fonte,data}
}
function construtorDespesas(valorDespesa, gasto, data, pagamento, parcelas,array) {
  array.push(factoryDespesas(valorDespesa, gasto, data, pagamento, parcelas));
}

function construtoReceitas(valorResceita,fonte,data){
    receita.push(factoryReceitas(valorResceita,fonte,data));
}

function contabilidade(){
const somaReceita = receita.reduce((total, e) => total + e.valor, 0);
const somaDespesas = despesas.reduce((total,e)=> total + e.valor,0);
let resultado = somaReceita - somaDespesas;
resultado < 0 ? console.log(`**********************\nSeu saldo está negativo \n---------------- \nSALDO: ${resultado}\n**********************`): 
console.log(`**********************\nSeu saldo está estável\n---------------- \nSALDO: ${resultado}\n**********************`);
}


module.exports ={construtorDespesas,construtoReceitas,contabilidade,receita,despesas}